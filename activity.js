// 1. What directive is used by Node.js in loading the modules it needs?

	require 

// 2. What Node.js module contains a method for server creation?

	http

// 3. What is the method of the http object responsible for creating a server using Node.js?

	http.createServer((request, response) => {/*We write responses here with respect to each request*/})

// 4. What method of the response object allows us to set status codes and content types?

	response.writehead(/*Status code is written here*/,{"Content-Type": /*content type is written here*/})


// 5. Where will console.log() output its contents when run in Node.js?
	
	It is shown in the Git Bash when we run the js file 

// 6. What property of the request object contains the address's endpoint?
	
	url property of request contains the address endpoint


